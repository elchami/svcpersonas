"""
Modulo Personas
"""

# System modules
from datetime import datetime
import os.path
from os import path
# 3rd party modules
from flask import make_response, abort
import json

def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))

#Proceso
def crearPersona(documento, nombre, apellido, sexo, edad):
    persona = {}
    persona["documento"]=documento
    persona["nombre"]=nombre
    persona["apellido"]=apellido
    persona["sexo"]=sexo
    persona["edad"]=edad
    return persona

def leerPersonas():
    persona=[]
    if (path.exists("personas1.json")):
        file = open("personas1.json","r")
        persona=json.loads(file.read())
    return persona

def escribirJson(persona):
    file = open("personas1.json","w")
    file.write(json.dumps(persona, indent=4))
    file.close()

#Operacion 1
def agregarPersona(alguien):
    documento = alguien.get("documento", None)
    nombre    = alguien.get("nombre", None)
    apellido  = alguien.get("apellido", None)
    sexo      = alguien.get("sexo", None)
    edad      = alguien.get("edad", None)
    persona=leerPersonas()
    persona.append(crearPersona(documento, nombre, apellido, sexo, edad))
    escribirJson(persona)

#Operacion 2
def crearHijo(documentoPadre, documentoHijo, nombre, apellido, sexo, edad):
    hijo=crearPersona(documentoHijo, nombre, apellido, sexo, edad)
    personas=leerPersonas()
    for persona in personas:
        if persona["documento"] == documentoPadre:
            if "Hijos" not in persona:
                persona["Hijos"]=[]
            persona["Hijos"].append(hijo)
            escribirJson(personas)


